package com.aop.aspects;

import java.util.Arrays;

import com.aop.Calculator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;

public aspect CalculatorLoggingWithAspectJ {
    private static final Logger logger = LogManager.getLogger(Calculator.class);

    pointcut Calculations()
        : within(com.aop.Calculator) && execution(public !static * *(..));

    before() : Calculations() {
        logger.trace("{AspectJ}: > " + this.buildMethodCallString(thisJoinPoint));
    }

    after() returning(Object retVal) : Calculations() {
        String ret = (retVal == null) ? "NULL" : retVal.toString();
        logger.trace("{AspectJ}: < " + this.buildMethodCallString(thisJoinPoint) +
                     " = " + ret);
    }

    private String buildMethodCallString(JoinPoint joinPoint) {
        String fnSig = joinPoint.getSignature().toLongString();
        String args = this.buildArgsString(joinPoint.getArgs());
        return fnSig + " : [ " + args + " ]";
    }

    // ... //

    private String buildArgsString(Object[] args) {
        String[] argsArr =
            Arrays.stream(args).map(Object::toString).toArray(String[] ::new);
        return String.join(", ", argsArr);
    }
}
