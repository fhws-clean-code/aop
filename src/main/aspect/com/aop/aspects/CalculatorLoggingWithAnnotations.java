package com.aop.aspects;

import java.util.Arrays;

import com.aop.Calculator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class CalculatorLoggingWithAnnotations {
    private Logger logger = LogManager.getLogger(Calculator.class);

    @Pointcut("within(com.aop.Calculator) && execution(public !static * *(..))")
    public void Calculations() {}

    @Before("Calculations()")
    public void traceEntry(JoinPoint joinPoint) {
        logger.trace("{Java}: > " + this.buildMethodCallString(joinPoint));
    }

    @AfterReturning(pointcut = "Calculations()", returning = "retVal")
    public void traceExit(JoinPoint joinPoint, Object retVal) {
        String ret = (retVal == null) ? "NULL" : retVal.toString();
        logger.trace("{Java}: < " + this.buildMethodCallString(joinPoint) + " = " + ret);
    }

    private String buildMethodCallString(JoinPoint joinPoint) {
        String fnSig = joinPoint.getSignature().toLongString();
        String args = this.buildArgsString(joinPoint.getArgs());
        return fnSig + " : [ " + args + " ]";
    }

    // ... //

    private String buildArgsString(Object[] args) {
        String[] argsArr =
            Arrays.stream(args).map(Object::toString).toArray(String[]::new);
        return String.join(", ", argsArr);
    }
}
