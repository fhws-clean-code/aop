package com.aop;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        Calculator calculator = new Calculator();

        try {
            double ans = calculator.divide(5, 7);
            System.out.println("5 / 7 = " + ans);
        } catch (ArithmeticException e) {
            System.err.println(e.getMessage());
        }

        testWeirdLogLevel();
    }

    public static void testWeirdLogLevel() {
        Level myWeirdLevel = Level.forName("Weird", 350);
        logger.log(myWeirdLevel, "Something weird happened");
    }
}
