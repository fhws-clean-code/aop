package com.aop;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Calculator {
    private static final Logger logger = LogManager.getLogger(Calculator.class);

    public Calculator() { super(); }

    public double add(double summandA, double summandB) {
        // NOTE: Here are some scattered logger calls. The code would look much
        //       cleaner if they were located in an aspect

        logger.entry(summandA, summandB);

        double ans = summandA + summandB;

        logger.exit(ans);
        return ans;
    }

    public double subtract(double minuend, double subtrahend) {
        // NOTE: Here are some scattered logger calls. The code would look much
        //       cleaner if they were located in an aspect

        logger.entry(minuend, subtrahend);

        double ans = minuend - subtrahend;

        logger.exit(ans);
        return ans;
    }

    public double multiply(double factorA, double factorB) {
        // NOTE: Could you imagine what a mess this would be if there were
        //       logger calls all over the place?

        if (factorA < 0) {
            factorA = subtract(0, factorA);
            factorB = subtract(0, factorB);
        }

        double product = 0;
        for (; factorA > 0; factorA--) {
            product = add(product, factorB);
        }

        return product;
    }

    public double divide(double dividend, double divisor) throws ArithmeticException {
        // NOTE: Using the logging aspects, we can log calls to this method
        //       without having to put a single logger call here :)

        if (divisor == 0) {
            throw new ArithmeticException("Division by zero is not allowed!");
        }

        return dividend / divisor;
    }
}
