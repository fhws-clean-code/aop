# AspectJ Example


## 🛠 Development Setup


### Build with Docker

Install [Docker](https://docs.docker.com/get-docker/) and
[Docker-Compose](https://docs.docker.com/compose/install/).

Then, from the project root, run `./mvnd clean install exec:java`. (On
Windows run `.\mvnd.cmd clean install exec:java`)

After some dependency installation and build output, you should finally see an
output like below:

    5 / 7 = 0.7142857142857143

To clean everything up (including the maven data volume) run `docker-compose
down -v`.



### Build using Maven Wrapper

Install Java 8, thenfrom the project root, run `./mvnw clean install exec:java`
(On Windows run `.\mvnw.cmd clean install exec:java`)

After some dependency installation and build output, you should finally see an
output like below:

    5 / 7 = 0.7142857142857143



## View Log Files

The log files are located under the `./logs` directory (the directory is
created during the first run of the application).

An example log file output should look like this:

    [TRACE] 2020-06-15 15:31:08 [main] com.aop.Calculator: {AspectJ}: > public double com.aop.Calculator.divide(double, double) : [ 5.0, 7.0 ]
    [TRACE] 2020-06-15 15:31:08 [main] com.aop.Calculator: {Java}: > public double com.aop.Calculator.divide(double, double) : [ 5.0, 7.0 ]
    [TRACE] 2020-06-15 15:31:08 [main] com.aop.Calculator: {Java}: < public double com.aop.Calculator.divide(double, double) : [ 5.0, 7.0 ] = 0.7142857142857143
    [TRACE] 2020-06-15 15:31:08 [main] com.aop.Calculator: {AspectJ}: < public double com.aop.Calculator.divide(double, double) : [ 5.0, 7.0 ] = 0.7142857142857143
    [Weird] 2020-06-15 15:31:08 [main] com.aop.App: Something weird happened
